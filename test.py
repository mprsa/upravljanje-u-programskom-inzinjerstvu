import networkx as nx
import igraph as ig
import graph_tool.all as gt
import louvain
import matplotlib.pyplot as plt
import numpy as np
import community
import os
import pdb
import tablib
from collections import OrderedDict



#graph = nx.Graph()
#graph.add_edge(1, 2)
#print(graph.number_of_nodes())

#G = nx.erdos_renyi_graph(30, 0.05)

#partition = community.best_partition(G)


def drawGraph(G, partition):
	#drawing


	size = float(len(set(partition.values())))
	pos = nx.spring_layout(G)
	count = 0.
	for com in set(partition.values()) :
	    count = count + 1.
	    list_nodes = [nodes for nodes in partition.keys()
	                                if partition[nodes] == com]
	    nx.draw_networkx_nodes(G, pos, list_nodes, node_size = 20,
	                                node_color = str(count / size))


	nx.draw_networkx_edges(G,pos, alpha=0.5)
	plt.show()


def parseData(fileName, filePath='', dataStartLine=0, column=0):

	fullFileName = fileName if filePath == '' else filePath + '/' + fileName
	file = open(fullFileName, 'r')
	data = file.readlines()
	rows = [element.split('\n') for element in data]
	first = rows[0][0]
	rows = rows[1:]
	rowsCleaned = [element for row in rows for element in row if element != '']
	data = [tuple(element.split()) for element in rowsCleaned]
	for index, element in enumerate(data):
		data[index] = tuple(map(int, element))
	#data2 = data.split('\n')
	#print(data)

	return data, int(first)

filePath = 'Datoteke/PDE/'
#filePath = os.getcwd() + '/' + filePath 
fileName = '4_3.txt'

edges, verticesNumber = parseData(fileName, filePath)
source= []
target = []
for element in edges:
	#print(element)
	try:
		source.append(element[0])
	except IndexError:
		target.append('')
	try:
		target.append(element[1])
	except IndexError:
		target.append('')
#print(edges)


#part of networkX
GNetworkxx = nx.DiGraph()
GNetworkxx.add_edges_from(edges)
UndirectedGNetworkx = GNetworkxx.to_undirected()
#print(G.edges())
partitionNetworkx = community.best_partition(UndirectedGNetworkx)
modularityNetworkX = community.modularity(partitionNetworkx, UndirectedGNetworkx)
#drawGraph(UndirectedGNetworkx, partitionNetworkx)


#part of igraph
edgeInt = []
GIgraph = ig.Graph(directed=True)
'''for index, edge in enumerate(edges):
		for vertex in edge:
		print(type(edge[index][0]))
		edgeInt[index][0] = int(edge[index][0])
		edgeInt[index][1] = int(edge[index][1])

		edgeInt[index][0] = edgeInt[index][0] - 1
		edgeInt[index][1] = edgeInt[index][1] - 1'''


#for i in range(verticesNumber + 1):
	#GIgraph.add_vertex(i)
edges2 = []
for edge in edges:
	edges2.append((edge[0] - 1, edge[1] - 1))
GIgraph.add_vertices(verticesNumber)
#GIgraph.delete_vertices(0)
GIgraph.add_edges(edges2)
#ig.summary(GIgraph)
#print(GIgraph)
partitionIgraph = louvain.find_partition(GIgraph, method='Modularity')
modulIgraph = GIgraph.modularity(partitionIgraph)
#drawGraph(GIgraph, partitionIgraph)

def dataParseGraph(G, graphLibrary, modul):
	data = OrderedDict()
	data['Broj cvorova'] = G.order() 	if graphLibrary==1 else G.vcount()
	data['Broj rubova']  = G.size()  	if graphLibrary==1 else G.ecount()
	data['Dijametar']    = '' 			if graphLibrary==1 else G.diameter(directed=True)
	#eigenvectorDict = nx.eigenvector_centrality(G) if graphLibrary==1 else ""
	#eigenvectrlist = []
	#print(type(eigenvectorDict))
	#for key, value in eigenvectorDict.items():
		#eigenvectrlist.append(value)
	#print(eigenvectorDict)
	#data['Eigenvektorova sredisnjost'] = reduce()
	data['Gustoca grafa']  = nx.density(G) if graphLibrary==1 else G.density()
	data['modularnost'] = modul[1]
	data['koeficijent zajednice'] = nx.average_clustering(G) if graphLibrary==1 else G.transitivity_undirected()
	#countShortestPaths = nx.shortest_path_length(G).values()
	#data['broj najkracih puteva'] = len(countShortestPaths)
	#data['Prosjecan duljinca puta'] = nx.all_simple_paths(G)

	print(data)

modul = []
modul.append([partitionNetworkx, modularityNetworkX])
modul.append([partitionIgraph, modulIgraph])

#dataParseGraph(UndirectedGNetworkx, 1, modul[0])
#dataParseGraph(GIgraph, 2, modul[1])

#graph_tool
#print(edges, verticesNumber)
vertices = []
[vertices.append(item) for item in element if item not in vertices]
#print(vertices)


#build our graph
graph = gt.Graph(directed=True)
#print(vertices)
for vertex in vertices:
	graph.add_vertex()
for index, edge in enumerate(edges2):
	graph.add_edge(edge[0], edge[1])


'''
#build our graph 2
graph = gt.Graph()
#graph.add_vertex(verticesNumber)
for index, edge in enumerate(edges):
	graph.add_edge(edge[0], edge[1])
'''

#print(graph.is_directed())
#YOLO
#def dataParseGraphGraphTool(G):
#graph = gt.load_graph_from_csv("1_0.csv",directed=False,  skip_first=True)
G = graph
print(G)
data = OrderedDict()
data['koeficijent zajednice'] = gt.global_clustering(G)
data['eigenvector'] = gt.eigenvector(G)
#data['modularity'] = gt.modularity(G, G.vp)

#state
#state_ndc_nn = gt.minimize_blockmodel_dl(G, deg_corr=False)
state_dc_nn = gt.minimize_blockmodel_dl(G, deg_corr=True)
#state_ndc = gt.minimize_nested_blockmodel_dl(G, deg_corr=False)
#state_dc = gt.minimize_nested_blockmodel_dl(G, deg_corr=True)
hierarchy = 0

'''
#entropy calculation - which model is the best
entropy = [state_ndc_nn.entropy(), state_dc_nn.entropy(), 
			state_ndc.entropy(), state_dc.entropy()]
print(entropy)
if (entropy.index(min(entropy)) == 0):
	state = state_ndc_nn
	hierarchy = 0
elif (entropy.index(min(entropy)) == 1):
	state = state_dc_nn
	hierarchy = 0
elif (entropy.index(min(entropy)) == 2):
	state = state_ndc
else:
 	state = state_dc
print(state.entropy())
'''



#print("state entropy:", state_dc.entropy())
#when we know the correct model in advance
state = state_dc_nn
#state = gt.minimize_blockmodel_dl(G, deg_corr=True)

# just a comparision
'''
g = gt.collection.data["power"]
state = gt.minimize_blockmodel_dl(g, deg_corr=True)
BlockState = state.get_blocks()
state.draw(vertex_shape=BlockState, output="power.pdf")
state = gt.minimize_nested_blockmodel_dl(g, deg_corr=True)
state.draw(output="powerNestedAll.pdf")
'''


# HIEARCHICAL
if hierarchy:
	modularityList = []
	previusGraph = G
	levels = state.get_levels()
	#BlockState = levels[1].get_blocks()
	#for e in BlockState:
	#	print(e)

	bs = state.get_bs()                     # Get hierarchical partition.
	bs += [np.zeros(1)] * (10 - len(bs))    # Augment it to L = 10 with
	print(bs)                               # single-group levels
	#state.draw(output="NestedAll.pdf")
	# EQUILIBRATE
	state = state.copy(bs = bs, sampling=True)
	gt.mcmc_equilibrate(state, wait=10, mcmc_args=dict(niter=10))

	for index, state in enumerate(levels):
		# block state with correct graph
		BlockState = state.get_blocks()
		# next graph
		blockGraph = state.get_bg()
		# block with next graph
		blockStateGraph = state.get_block_state()
		setBlockState = set(BlockState)
		#print("state: ", state, setBlockState, blockGraph, blockStateGraph)
		print("state: ", state, previusGraph)
		modularityList.append(gt.modularity(previusGraph, BlockState))
		previusGraph = blockGraph
		#state.draw(output="NestedStateEquiibrate_%i.pdf" % index)
	print(modularityList)
#NORMAL
else:
	#BlockState = state.get_blocks()
	#state.draw(vertex_shape=BlockState, output="normal_1_0_JDT.pdf")
	#state = state.copy(B=G.num_vertices())
	#state.draw(vertex_shape=BlockState, output="normalCopy.pdf")
	#dS, nmoves = state.mcmc_sweep(niter=1000)
	#gt.mcmc_equilibrate(state, wait=1000, mcmc_args=dict(niter=10))
	exten = ["pdf", "xml"]
	exte_fold = ["pdf2", "xml2"]
	name = "4_3_PDE"
	nor = name + "_" + "normal"
	sweep = name + "_" + "sweep"
	eq = name + "_" + "equilibrate"
	BlockState = state.get_blocks()
	state.draw(vertex_shape=BlockState, vorder=BlockState, vertex_text=G.vertex_index, vertex_font_size=1,
	output=exte_fold[0] + "/" +  nor + "." + exten[0])
	print("1_0", gt.modularity(G, BlockState))
	print(BlockState)




BlockState = state.get_blocks()
#for e in BlockState:
	#print(e)

groupMembership = []
for index, vertex in enumerate(G.vertices()):
	groupMembership.append(BlockState[index])
	#print(groupMembership[index], vertex)
#print(groupMembership)

# create unordered collection of a DISTINCT objects.
numberOfGroups = set(groupMembership)
numberOfCommunities = len(numberOfGroups)
#print(numberOfGroups)

vprop = G.new_vertex_property("int")
G.vp.zajednica = vprop


for index, vertex in enumerate(G.vertices()):
	G.vp.zajednica[vertex] = groupMembership[index]
	#print(G.vp.group[vertex], groupMembership[index])
	#print("cvor: ", vertex, " zajednica: ", G.vp.zajednica[vertex])

gprop = G.new_graph_property("string")
G.gp.koeficijent_zajednice = gprop
G.gp.koeficijent_zajednice = data['koeficijent zajednice']

gprop = G.new_graph_property("string")
G.gp.broj_zajednica = gprop
G.gp.broj_zajednica = numberOfCommunities

gprop = G.new_graph_property("string")
G.gp.modularnost = gprop
G.gp.modularnost = format(gt.modularity(G, BlockState), '.8f')


G.save(exte_fold[1] + "/" + nor + "." + exten[1])

'''
#vertexPropertyMap = state.get_er()
#degreePropertyMap = G.degree_property_map(deg="total")

#for vertex in G.vertices():
	#print(vertex, b[vertex])
'''

#state.draw(vertex_shape=BlockState, output="ola2.pdf")
#state.draw(output="olaNested.pdf")
#print(gt.modularity(G, BlockState))
#BlockState or NestedBlockState

'''
#prepare for sweeps
if (hierarchy == 0):
	state = state.copy(B=G.num_vertices())
else:
	# Before doing model averaging, the need to create a NestedBlockState
	# by passing sampling = True.

	# We also want to increase the maximum hierarchy depth to L = 10

	# We can do both of the above by copying.

	# Get hierarchical partition.
	bs = state.get_bs()
	# Augment it to L = 10 with single-group levels.
	bs += [np.zeros(1) * (10 - len(bs))]
	state = state.copy(bs=bs, sampling=True)
#sweeps -> run 1000 sweeps of the MCMC
dS, nmoves = state.mcmc_sweep(niter=1000)
print("Change in description length:", dS)
print("Number of accepted vertex moves:", nmoves)
print("State entropy after averaging over models:", state.entropy())
'''


if (hierarchy == 0):
	pass
else:
	# We will accept equilibration if 10 sweeps are completed without a
	# record breaking event, 2 consecutive times.
	#gt.mcmc_equilibrate(state, wait=5, mcmc_args=dict(niter=10))
	#pv = [None] * len(state.get_levels())
	#print("prvi")


	'''The function mcmc_equilibrate() accepts a callback argument that 
	takes an optional function to be invoked after each call to mcmc_sweep().
	This function should accept a single parameter which will contain the 
	actual BlockState instance. We will use this in the example below to 
	collect the posterior vertex marginals, i.e. the posterior probability 
	that a node belongs to a given group:'''
	'''
	def collect_marginals(s):
		#print(pv)
			global pv
			#pv = [sl.collect_vertex_marginals(pv[l]) for l, sl in enumerate(s.get_levels())]
			for l, sl in enumerate(s.get_levels()):
				pv[l] = sl.collect_vertex_marginals(pv[l])

	# Now we collect the marginals for exactly 100,000 sweep
	gt.mcmc_equilibrate(state, force_niter=100, mcmc_args=
		dict(niter=10), callback=collect_marginals)
	print("drugi")
	'''

	# Now the node marginals for all levels are stored in property map 
	# propertyVertexvMap. We can visualize the first level as pie charts
	#on the nodes:
	#state_0 = state.get_levels()[0]
	'''state.draw(pos=G.vp.pos, vertex_shape="pie", vertex_pie_fractions=
		posteriorVertexMap, edge_gradient=None, output=
		"1_0-marginals.svg")
	'''

	'''print(state.print_summary())
	levels = state.get_levels()
	for s in levels:
		print(s)

	# marginal probabillty of the number of groups itself
	h = [np.zeros(G.num_vertices() + 1) for s in state.get_levels()]

	def collect_num_groups(s):
	    for l, sl in enumerate(s.get_levels()):
	       B = sl.get_nonempty_B()
	       h[l][B] += 1

	# Now we collect the marginal distribution for exactly 100,000 sweeps
	gt.mcmc_equilibrate(state, force_niter=100, mcmc_args=dict(niter=10),
	                    callback=collect_num_groups)

	print(state.print_summary())
	levels = state.get_levels()
	for s in levels:
		print(s)

	# hierarchical partitions sampled from the posterior distribution
	for i in range(2):
	    state.mcmc_sweep(niter=1000)
	    state.draw(output="1_0-%i.svg" % i, empty_branches=False)
	'''


#state.draw(output="1_0_non_nested.svg")
#state2.draw(output="1_0_non_nested_deg_corr.svg")
#state3.draw(output="1_0_nested.svg")
#state4.draw(output="1_0_nested_deg_corr.svg")


'''#edgeCountMatrix = state.get_matrix()
#mathsow(edgeCountMatrix.todense())
#savefig("1_0_non_nested_edge_count_matrix.svg")
edgeCountMatrixGraph = state.get_bg()
edgeCountsMatrixGraphEdges = edgeCountMatrixGraph.ep.count
edgeCountsMatrixGraphVertices = edgeCountMatrixGraph.vp.count
data['edges'] = edgeCountsMatrixGraphEdges
data['vertices'] = edgeCountsMatrixGraphVertices'''
#data['modularity'] = gt.modularity(G, G.vp)


'''
dataFootball = 	gt.collection.data['football']
print(dataFootball)
dataFootball.save("football.graphml")
#print(dataFootball.list_properties())



#print(dataFootball.vp["value_tsevans"])
#print(gt.modularity(dataFootball, dataFootball.vp.value))
#
#state = gt.minimize_blockmodel_dl(dataFootball, deg_corr=True)
#state = gt.minimize_nested_blockmodel_dl(dataFootball, deg_corr=True)
BlockState = state.get_blocks()
#vertexPropertyMap = state.get_er()
#for vertex in G.vertices():
	#print(vertex, b[vertex])
state.draw(vertex_shape=BlockState, output="football.pdf")
print("value: ", gt.modularity(dataFootball, dataFootball.vp.value))
print("value_tsevans: ", gt.modularity(dataFootball, dataFootball.vp.value_tsevans))
'''
#state.draw(output="FootballNested.pdf")
#print(gt.modularity(dataFootball, vertexPropertyMap))

#print(data)

#dataParseGraphGraphTool(graph)

# neighbours dont have anything to do with propertyMap used in moudlarity

'''
value_tsevansList = []
valueList = []
in_degreeList = []
out_degreeList = []
print("value: ", gt.modularity(dataFootball, dataFootball.vp.value))
print("value_tsevans: ", gt.modularity(dataFootball, dataFootball.vp.value_tsevans))
state = gt.minimize_blockmodel_dl(dataFootball, deg_corr=True)
gt.mcmc_equilibrate(state, wait=10, mcmc_args=dict(niter=1000))
BlockState = state.get_blocks()
print("one repeat" , gt.modularity(dataFootball, BlockState))
'''
#for index, vertex in enumerate(dataFootball.vertices()):
	#value_tsevansList.append(dataFootball.vp.value_tsevans[vertex])
	#valueList.append(dataFootball.vp.value[vertex])
	#in_degreeList.append([v for v in dataFootball.get_in_neighbours(index)])
	#out_degreeList.append([v for v in dataFootball.get_out_neighbours(index)])
#print("value_tsevansList", value_tsevansList)
#print("valueList", valueList)
#print("set value_tsevans", set(value_tsevansList))
#print("set value", set(valueList))
#print(in_degreeList)
#print(out_degreeList)
#v = dataFootball.vertex(index)
#print(dataFootball.vp.value_tsevans[v])

'''
vertices = dataFootball.get_vertices()
edges = dataFootball.get_edges()
trialGraph = gt.Graph(directed=True)
lst = []
for e in dataFootball.edges():
	#print(e)
	lst.append(e)
	#print(lst)
	#trialGraph.add_edge(e[0], e[1])
#print(lst)
#print(dataFootball.get_edges())
edges = dataFootball.get_edges()
for e in edges:
	#print(e)
	trialGraph.add_edge(e[0], e[1])
#for edge in edges:
	#print(edges)
	#trialGraph.add_edge(edge[0], edge[1])

print(trialGraph)
trialGraph.save("trialFootball.graphml")

state = gt.minimize_blockmodel_dl(trialGraph, deg_corr=True)
#state = state.copy(B=dataFootball.num_vertices())
#gt.mcmc_equilibrate(state, wait=10, mcmc_args=dict(niter=10))
BlockState = state.get_blocks()
print("trialGraph modularity" , gt.modularity(trialGraph, BlockState))
'''